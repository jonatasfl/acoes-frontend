# Frontend (teste)

Frontend de um teste para vaga de Engenheiro de software.
Neste teste optei por utilizar o **React.js**, pois além de ser uma biblioteca que venho utilizando bastante utlimamente, é a mais utilizada para construção de interfaces e Single Page Aplications (SPA), além de ser possível utilizar os conhecimentos no desenvolvimento mobile, através do **React Native**.

Utilizei também o **Typescript** ao invés do Javascript, pois facilita a leitura do código, devido à tipagem dos dados.

Não utilizei **Redux**, pois não houve a necessidade.

## Instalação

Antes de tudo, configurar os parâmetros no arquivo `src/config.ts`.
Para executar, basta usar os comandos abaixo:

    npm install
    npm start
