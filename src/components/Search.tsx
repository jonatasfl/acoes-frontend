import React, { useState } from 'react';
import { RouteComponentProps, withRouter } from 'react-router-dom';

import { MdSearch } from 'react-icons/md';

const Search: React.FC<RouteComponentProps> = ({ history }) => {
  const [symbol, setSymbol] = useState('');

  function handleSubmit(e: React.FormEvent<HTMLFormElement>): void {
    e.preventDefault();
    history.push(`/quote/${symbol}`);
  }

  return (
    <form className="form-inline form-search" onSubmit={handleSubmit}>
      <div className="form-group mr-2">
        <input
          type="text"
          className="form-control form-control-lg"
          placeholder="Search for symbol"
          value={symbol}
          onChange={(e) => setSymbol(e.target.value.toUpperCase())}
        />
      </div>
      <div className="form-group">
        <button type="submit" className="btn btn-primary btn-lg">
          <MdSearch />
        </button>
      </div>
    </form>
  );
};

export default withRouter(Search);
