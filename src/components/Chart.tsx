import React, { useState, useEffect } from "react";
// import ReactDOM from 'react-dom';
import { AreaChart, Area, CartesianGrid, XAxis, YAxis, Tooltip, ResponsiveContainer } from "recharts";
// import { MdFullscreen } from 'react-icons/md';

import StockService from "../services/api/StockService";
import IChart from "../interfaces/IChart";
import MChart from "../models/Chart";

const Chart: React.FC<{ symbol: string }> = ({ symbol }) => {
  const [chartData, setChartData] = useState<[IChart]>([MChart]);
  const [chartPeriod, setChartPeriod] = useState<string>("1m");
  // const [fullscreen, setFullscreen] = useState<boolean>(false);

  useEffect(() => {
    getChart();
  }, [chartPeriod, symbol]);

  async function getChart() {
    try {
      const data = await StockService.getChart(symbol, chartPeriod);
      setChartData(data.data);
    } catch (e) {
      alert("Error getting chart data.");
    }
  }

  const chart = (
    <>
      <div id="chart-controls" className="row">
        <div className="col-12 text-center">
          <div className="btn-group">
            <button type="button" className="btn btn-link" onClick={() => setChartPeriod("5d")}>
              5D
            </button>
            <button type="button" className="btn btn-link" onClick={() => setChartPeriod("1m")}>
              1M
            </button>
            <button type="button" className="btn btn-link" onClick={() => setChartPeriod("3m")}>
              3M
            </button>
            <button type="button" className="btn btn-link active" onClick={() => setChartPeriod("6m")}>
              6M
            </button>
            <button type="button" className="btn btn-link" onClick={() => setChartPeriod("ytd")}>
              YTD
            </button>
            <button type="button" className="btn btn-link" onClick={() => setChartPeriod("1y")}>
              1Y
            </button>
            <button type="button" className="btn btn-link" onClick={() => setChartPeriod("5y")}>
              5Y
            </button>
            <button type="button" className="btn btn-link" onClick={() => setChartPeriod("max")}>
              MAX
            </button>
          </div>
        </div>
        {/* <div className="col-2 text-right p-0">
          <button
            type="button"
            className="btn btn-link p-0"
            title="Enable fullscreen mode"
            data-toggle="tooltip">
            <MdFullscreen />
          </button>
        </div> */}
      </div>
      <div id="chart-row" className="row">
        <div className="col-12">
          <ResponsiveContainer>
            <AreaChart data={chartData}>
              <Area type="monotone" dataKey="close" stroke="#8884d8" />
              <CartesianGrid strokeDasharray="3 3" />
              <XAxis dataKey="label" />
              <YAxis />
              <Tooltip />
            </AreaChart>
          </ResponsiveContainer>
        </div>
      </div>
    </>
  );

  /*
  const chartContainerEl = document.getElementById('chart-container')
  || document.createElement('div');
  const fsContent = document.getElementById('fs-modal-body') || document.createElement('div');
  const container = fullscreen ? fsContent : chartContainerEl;
  return ReactDOM.createPortal(chart, container);
  */

  return chart;
};

export default Chart;
