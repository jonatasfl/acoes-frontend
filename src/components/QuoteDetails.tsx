import React from 'react';

import IStock from '../interfaces/IStock';

const QuoteDetails: React.FC<{ stock: IStock }> = ({ stock }) => (
  <table id="tbl-quote-details" className="table table-sm table-borderless">
    <tbody>
      <tr>
        <td>Previous Close</td>
        <th>{stock.previousClose}</th>
      </tr>
      <tr>
        <td>Market Cap</td>
        <th>{stock.marketCap.toLocaleString('en-US', { style: 'currency', currency: 'USD' })}</th>
      </tr>
      <tr>
        <td>Open</td>
        <th>{stock.open}</th>
      </tr>
      <tr>
        <td>52 Week Range</td>
        <th>{`${stock.week52Low} - ${stock.week52High}`}</th>
      </tr>
      <tr>
        <td>Avg. Volume</td>
        <th>{stock.avgTotalVolume.toLocaleString('en-US')}</th>
      </tr>
      <tr>
        <td>PE Ratio</td>
        <th>{stock.peRatio}</th>
      </tr>
      <tr>
        <td>Ask</td>
        <th>{stock.iexAskPrice}</th>
      </tr>
      <tr>
        <td>Bid</td>
        <th>{stock.iexBidPrice}</th>
      </tr>
      <tr>
        <td>YTD Change</td>
        <th>{stock.ytdChange}</th>
      </tr>
    </tbody>
  </table>
);

export default QuoteDetails;
