import React from 'react';
import ReactDOM from 'react-dom';

const FSModal: React.FC = () => (
  <div className="modal fade" id="fsModal" tabIndex={-1} role="dialog" aria-labelledby="fsModalLabel" aria-hidden="true">
    <div className="modal-dialog modal-xl modal-dialog-centered" role="document">
      <div className="modal-content">
        <div id="fs-modal-body" className="modal-body" style={{ minHeight: '300px' }} />
      </div>
    </div>
  </div>
);

export default FSModal;
