import React from 'react';
import ICompany from '../interfaces/ICompany';

const CompanyDetails: React.FC<{ company: ICompany }> = ({ company }) => (
  <div className="company-details">
    <h5>{company.companyName}</h5>

    <div className="row pt-4">
      <div className="col-6 d-flex flex-column">
        <span>{company.address}</span>
        <span>{`${company.city}, ${company.state} ${company.zip}`}</span>
        <span>{company.country}</span>
        <span>{company.phone}</span>
        <span>
          <a href={company.website} target="_blank" rel="noopener noreferrer">
            {company.website}
          </a>
        </span>
      </div>
      <div className="col-6 d-flex flex-column">
        <span>
          Sector: <strong>{company.sector}</strong>
        </span>
        <span>
          Industry: <strong>{company.industry}</strong>
        </span>
        <span>
          Employees: <strong>{company.employees.toLocaleString('en-US')}</strong>
        </span>
        <span>
          CEO: <strong>{company.CEO}</strong>
        </span>
      </div>
    </div>
    <hr />
    <div className="row">
      <div className="col-12 text-justify">{company.description}</div>
    </div>
  </div>
);

export default CompanyDetails;
