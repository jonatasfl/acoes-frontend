export default {
  date: "",
  high: 0,
  low: 0,
  volume: 0,
  open: 0,
  close: 0,
  uHigh: 0,
  uLow: 0,
  uVolume: 0,
  uOpen: 0,
  uClose: 0,
  changeOverTime: 0,
  label: 0,
  change: 0,
  changePercent: 0
};
