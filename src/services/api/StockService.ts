import { AxiosPromise } from 'axios';
import BaseApiConfig from './BaseApiConfig';

import IStock from '../../interfaces/IStock';
import IChart from '../../interfaces/IChart';
import ICompany from '../../interfaces/ICompany';

class StockService extends BaseApiConfig {
  getQuote(symbol: string): AxiosPromise<IStock> {
    return this.api.get(`/stock/${symbol}/quote`);
  }

  getChart(symbol: string, period = '1m'): AxiosPromise<[IChart]> {
    return this.api.get(`/stock/${symbol}/chart/${period}`);
  }

  getCompany(symbol: string): AxiosPromise<ICompany> {
    return this.api.get(`/stock/${symbol}/company`);
  }

  getQuoteField(symbol: string, field: string): AxiosPromise {
    return this.api.get(`/stock/${symbol}/quote/${field}`);
  }
}

export default new StockService();
