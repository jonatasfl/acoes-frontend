import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import config from '../../config';

export default class BaseApiConfig {
  api: AxiosInstance;

  constructor() {
    this.api = axios.create({
      baseURL: config.IEX_SANDBOX_MODE ? config.IEX_SANDBOX_API_BASE_URL : config.IEX_API_BASE_URL,
    });

    this.api.interceptors.request.use((req: AxiosRequestConfig) => {
      req.params = {};
      req.params.token = config.IEX_SANDBOX_MODE ? config.IEX_SANDBOX_API_TOKEN : config.IEX_API_TOKEN;

      return req;
    });
  }
}
