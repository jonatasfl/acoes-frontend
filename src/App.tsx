import React from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";

import Home from "./pages/Home";
import Quote from "./pages/Quote";

import "./assets/scss/main.scss";

const App: React.FC = () => (
  <BrowserRouter>
    <div className="app">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/quote/:symbol" component={Quote} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default App;
