import React, { useEffect, useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import ReactLoading from 'react-loading';
import Switch from 'react-switch';

import Search from '../components/Search';
import QuoteDetails from '../components/QuoteDetails';
import CompanyDetails from '../components/CompanyDetails';
import Chart from '../components/Chart';
import { formatChange, isPositive, toPercent } from '../helpers/MyHelpers';
import StockService from '../services/api/StockService';
import IStock from '../interfaces/IStock';
import ICompany from '../interfaces/ICompany';
import MStock from '../models/Stock';
import MCompany from '../models/Company';

const Quote: React.FC<IProps> = ({ match }) => {
  const [loading, setLoading] = useState(true);
  const [symbol, setSymbol] = useState<string>(match.params.symbol);
  const [stock, setStock] = useState<IStock>(MStock);
  const [company, setCompany] = useState<ICompany>(MCompany);
  const [realtime, setRealtime] = useState(false);
  const [error, setError] = useState('');

  useEffect(() => {
    getData();
  }, [symbol]);

  useEffect(() => {
    setSymbol(match.params.symbol);
  }, [match.params.symbol]);

  useEffect(() => {
    document.title = `${stock.symbol} ${stock.latestPrice.toFixed(2)} ${stock.change} ${toPercent(stock.changePercent)}`;
  }, [stock.change, stock.changePercent, stock.latestPrice, stock.symbol, symbol]);

  useEffect(() => {
    getRealtimeData();
  }, [realtime]);

  async function getData(): Promise<void> {
    try {
      const data = await Promise.all([
        StockService.getQuote(symbol.toLowerCase()),
        StockService.getCompany(symbol),
      ]);
      setStock(data[0].data);
      setCompany(data[1].data);
      setLoading(false);
    } catch (e) {
      if (e.response.status === 404) {
        setError('Symbol not found or not supported.');
      } else {
        setError('An error ocurred while processing your request.');
      }

      setLoading(false);
    }
  }

  function getRealtimeData() {
    if (realtime) {
      setInterval(async () => {
        const response = await Promise.all([
          StockService.getQuoteField(symbol, 'latestPrice'),
          StockService.getQuoteField(symbol, 'change'),
          StockService.getQuoteField(symbol, 'changePercent'),
        ]);

        setStock({
          ...stock,
          latestPrice: response[0].data,
          change: response[1].data,
          changePercent: response[2].data,
        });
      }, 2000);
    }
  }

  return (
    <div className="quote-page">
      <div id="search-container" className="container">
        <Search />
      </div>

      <div className="container">
        {loading && (
          <div className="w-100 d-flex justify-content-center">
            <ReactLoading color="#8884d8" type="bars" />
          </div>
        )}
        {error && (
          <div className="alert alert-danger" role="alert">
            {error}
          </div>
        )}
        {!error && !loading && (
          <>
            <header>
              <h1>
                {stock.symbol}
                <small>{stock.companyName}</small>
              </h1>
              <div className="d-flex flex-column align-items-center">
                <h6>{`Market ${stock.isUSMarketOpen ? 'Open' : 'Closed'}`}</h6>
                <small className="text-muted">{`Latest source: ${stock.latestSource} at ${stock.latestTime}`}</small>
                <span className="d-flex align-items-center pt-3">
                  <Switch onChange={() => setRealtime(!realtime)} checked={realtime} height={20} />
                  <small className="mx-2">{realtime ? 'Realtime enabled' : 'Realtime disabled'}</small>
                </span>
              </div>
              <div className="price">
                <h3>{`$${stock.latestPrice.toFixed(2)}`}</h3>
                <span className={isPositive(stock.change) ? 'positive-change' : 'negative-change'}>
                  {`${formatChange(stock.change)} (${formatChange(stock.changePercent, true)})`}
                </span>
              </div>
            </header>

            <hr />
            <ul className="nav nav-tabs">
              <li className="nav-item">
                <a className="nav-link active" href="#home" data-toggle="tab" role="tab">
                  Home
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#company" data-toggle="tab" role="tab">
                  Company
                </a>
              </li>
            </ul>
            <div className="tab-content">
              <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div className="row pt-3">
                  <div className="col-md-6">
                    <QuoteDetails stock={stock} />
                  </div>
                  <div className="col-md-6" style={{ display: 'block' }}>
                    <div id="chart-container" style={{ width: '100%', height: '100%' }}>
                      <Chart symbol={symbol} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="tab-pane fade pt-3" id="company" role="tabpanel" aria-labelledby="company-tab">
                <CompanyDetails company={company} />
              </div>
            </div>
          </>
        )}
      </div>
    </div>
  );
};

interface MatchParams {
  symbol: string;
}
interface IProps extends RouteComponentProps<MatchParams> {}

export default Quote;
