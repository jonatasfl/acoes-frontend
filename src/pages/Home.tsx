import React from "react";
import Search from "../components/Search";

const Home: React.FC = () => (
  <div className="home-page container w-60">
    <Search />
  </div>
);

export default Home;
