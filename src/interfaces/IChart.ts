export default interface IChart {
  date: string;
  high: number;
  low: number;
  volume: number;
  open: number;
  close: number;
  uHigh: number;
  uLow: number;
  uVolume: number;
  uOpen: number;
  uClose: number;
  changeOverTime: number;
  label: number;
  change: number;
  changePercent: number;
}
