/**
 * Converts a number to percent
 * @param {number} value - The value to be converted
 * @return {string} The string with %
 */
export function toPercent(value = 0): string {
  return `${(value * 100).toFixed(2)}%`;
}

/**
 * Format stock quote change
 * @param {number} value - The value to be formatted
 * @param {boolean} asPercent - If should return as percent
 * @return {string} The formatted number as string
 */
export function formatChange(value = 0, asPercent = false): string {
  const prefix = isPositive(value) ? '+' : '';
  return asPercent ? `${prefix}${toPercent(value)}` : `${prefix}${value}`;
}

/**
 * Verify if a number is positive
 * @param {number} value - The value to be verified
 * @return {boolean}
 */
export function isPositive(value = 0): boolean {
  return [0, 1].includes(Math.sign(value));
}
